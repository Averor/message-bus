<?php declare(strict_types=1);

namespace Averor\MessageBus\Tests\Fixtures;

use Averor\MessageBus\Contract\Dispatcher;
use Averor\MessageBus\Contract\Message;

/**
 * Class SampleCommandDispatcher
 *+
 * @package Averor\MessageBus\Tests\Fixtures
 * @author Averor <averor.dev@gmail.com>
 */
class SampleCommandDispatcher implements Dispatcher
{
    /** @var callable */
    private $handler;

    public function __construct(callable $handler)
    {
        $this->handler = $handler;
    }

    public function execute(Message $command, callable $next) : void
    {
        ($this->handler)($command);

        $next($command);
    }
}
