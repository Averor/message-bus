<?php declare(strict_types=1);

namespace Averor\MessageBus\Tests\Fixtures;

use Averor\MessageBus\Contract\Command;

/**
 * Class SecondSampleCommand
 *
 * @package Averor\MessageBus\Tests\Fixtures
 * @author Averor <averor.dev@gmail.com>
 */
class SecondSampleCommand implements Command
{
    /** @var int */
    public $id;

    /** @var string */
    public $name = self::class;

    public function __construct(int $id)
    {
        $this->id = $id;
    }
}
