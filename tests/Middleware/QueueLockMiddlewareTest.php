<?php declare(strict_types=1);

namespace Averor\MessageBus\Tests\Middleware;

use Averor\MessageBus\Contract\Command;
use Averor\MessageBus\MessageBus;
use Averor\MessageBus\Middleware\QueueLockMiddleware;
use Averor\MessageBus\Tests\Fixtures\FirstSampleCommand;
use Averor\MessageBus\Tests\Fixtures\SampleCommandDispatcher;
use Averor\MessageBus\Tests\Fixtures\SecondSampleCommand;
use PHPUnit\Framework\TestCase;

/**
 * Class QueueLockMiddlewareTest
 *
 * Test inspired by
 * @see https://github.com/SimpleBus/SimpleBus/blob/master/Component/MessageBus/tests/Bus/FinishesCommandBeforeHandlingNextTest.php
 *
 * @package Averor\MessageBus\Tests\Middleware
 * @author Averor <averor.dev@gmail.com>
 */
class QueueLockMiddlewareTest extends TestCase
{
    public function test_handling_wait_for_previous_to_finish() : void
    {
        $firstMessage = new FirstSampleCommand(10);
        $secondMessage = new SecondSampleCommand(20);

        $messageBus = new MessageBus([
            new QueueLockMiddleware()
        ]);

        $result = [];
        $messageBus->appendMiddleware(
            new SampleCommandDispatcher(
                function(Command $command) use (&$result, $messageBus, $secondMessage) {
                    $result[] = 'handling ' . get_class($command) . ' started';
                    if ($command instanceof FirstSampleCommand) {
                        $messageBus->dispatch($secondMessage);
                    }
                    $result[] = 'handling ' . get_class($command) . ' finished';
                }
            )
        );

        $messageBus->dispatch($firstMessage);

        $this->assertEquals(
            [
                sprintf('handling %s started', get_class($firstMessage)),
                sprintf('handling %s finished', get_class($firstMessage)),
                sprintf('handling %s started', get_class($secondMessage)),
                sprintf('handling %s finished', get_class($secondMessage))
            ],
            $result
        );
    }

    public function test_handling_does_not_wait_for_previous_to_finish_without_middleware() : void
    {
        $firstMessage = new FirstSampleCommand(10);
        $secondMessage = new SecondSampleCommand(20);

        $messageBus = new MessageBus();

        $result = [];
        $messageBus->appendMiddleware(
            new SampleCommandDispatcher(
                function(Command $command) use (&$result, $messageBus, $secondMessage) {
                    $result[] = 'handling ' . get_class($command) . ' started';
                    if ($command instanceof FirstSampleCommand) {
                        $messageBus->dispatch($secondMessage);
                    }
                    $result[] = 'handling ' . get_class($command) . ' finished';
                }
            )
        );

        $messageBus->dispatch($firstMessage);

        $this->assertEquals(
            [
                sprintf('handling %s started', get_class($firstMessage)),
                sprintf('handling %s started', get_class($secondMessage)),
                sprintf('handling %s finished', get_class($secondMessage)),
                sprintf('handling %s finished', get_class($firstMessage))
            ],
            $result
        );
    }
}
