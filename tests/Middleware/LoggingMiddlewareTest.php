<?php declare(strict_types=1);

namespace Averor\MessageBus\Tests\Middleware;

use Averor\MessageBus\Middleware\LoggingMiddleware;
use Averor\MessageBus\Tests\Fixtures\FirstSampleCommand;
use Averor\SimpleLogger\Logger;
use PHPUnit\Framework\TestCase;
use Psr\Log\LogLevel;

/**
 * Class LoggingMiddlewareTest
 *
 * @package Averor\MessageBus\Tests\Middleware
 * @author Averor <averor.dev@gmail.com>
 */
class LoggingMiddlewareTest extends TestCase
{
    public function test_it_does_write_log_info() : void
    {
        $messageBus = new \Averor\MessageBus\MessageBus([
            new LoggingMiddleware(
                new Logger('php://output'),
                LogLevel::WARNING
            )
        ]);

        ob_start();

        $messageBus->dispatch(
            $message = new FirstSampleCommand(10)
        );

        $result = rtrim(ob_get_clean(), PHP_EOL);

        $this->assertEquals(
            implode(PHP_EOL, [
                sprintf("warning Dispatching message %s", get_class($message)),
                sprintf("warning Message %s dispatched", get_class($message))
            ]),
            $result
        );
    }
}
