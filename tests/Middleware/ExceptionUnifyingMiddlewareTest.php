<?php declare(strict_types=1);

namespace Averor\MessageBus\Tests\Middleware;

use Averor\MessageBus\Contract\Command;
use Averor\MessageBus\Exception\MessageHandlingException;
use Averor\MessageBus\MessageBus;
use Averor\MessageBus\Middleware\ExceptionUnifyingMiddleware;
use Averor\MessageBus\Tests\Fixtures\FirstSampleCommand;
use Averor\MessageBus\Tests\Fixtures\SampleCommandDispatcher;
use PHPUnit\Framework\TestCase;

/**
 * Class ExceptionUnifyingMiddlewareTest
 *
 * @package Averor\MessageBus\Tests\Middleware
 * @author Averor <averor.dev@gmail.com>
 */
class ExceptionUnifyingMiddlewareTest extends TestCase
{
    public function test_it_throws_proper_exception() : void
    {
        $this->expectException(MessageHandlingException::class);

        $message = new FirstSampleCommand(10);

        $messageBus = new MessageBus([
            new ExceptionUnifyingMiddleware(),
            new SampleCommandDispatcher(
                function(Command $command)
                {
                    throw new \LogicException("exception test");
                }
            )
        ]);

        $messageBus->dispatch($message);
    }

    public function test_it_throws_original_exception_without_middleware() : void
    {
        $this->expectException(\LogicException::class);

        $message = new FirstSampleCommand(10);

        $messageBus = new MessageBus([
            new SampleCommandDispatcher(
                function(Command $command)
                {
                    throw new \LogicException("exception test");
                }
            )
        ]);

        $messageBus->dispatch($message);
    }
}
