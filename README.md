# Averor/Message-Bus

[![pipeline status](https://gitlab.com/Averor/message-bus/badges/master/pipeline.svg)](https://gitlab.com/Averor/message-bus/commits/master)
[![coverage report](https://gitlab.com/Averor/message-bus/badges/master/coverage.svg)](https://gitlab.com/Averor/message-bus/commits/master)
[![MIT License](https://img.shields.io/badge/license-MIT-brightgreen.svg)](https://gitlab.com/Averor/message-bus/blob/master/LICENSE)
[![Semver](http://img.shields.io/SemVer/2.0.0-pre-alpha.png)](http://semver.org/spec/v2.0.0.html)

Simple, convention based library aimed to help dealing with cqrs (with or without ES) tasks.
Heavily influenced by the best: [SimpleBus](https://github.com/SimpleBus/SimpleBus), [Tactician](https://github.com/thephpleague/tactician), [Broadway](https://github.com/broadway/broadway)

## Install using Composer:

`composer require averor/message-bus`

## Documentation

Will be surely written... someday. As for now - just take a look at [examples/](https://gitlab.com/Averor/message-bus/tree/master/examples)
or [tests/](https://gitlab.com/Averor/message-bus/tree/master/tests)

### QueryBus

MessageBus is not able to return value from handler, so that's where 
[QueryBus extension](https://gitlab.com/Averor/query-bus) comes to the rescue.

## Testing with PHPUnit (>=7.0)
`$ ./vendor/bin/phpunit ./tests` 
