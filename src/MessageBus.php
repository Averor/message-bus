<?php declare(strict_types=1);

namespace Averor\MessageBus;

use Averor\MessageBus\Contract\Message;
use Averor\MessageBus\Contract\MessageBus as IMessageBus;
use Averor\MessageBus\Contract\Middleware;

/**
 * Class MessageBus
 *
 * @package Averor\MessageBus
 * @author Averor <averor.dev@gmail.com>
 */
class MessageBus implements IMessageBus
{
    protected $middlewares;

    public function __construct(array $middlewares = [])
    {
        $this->middlewares = array_reverse($middlewares);
    }

    public function appendMiddleware(Middleware $middleware) : void
    {
        array_unshift($this->middlewares, $middleware);
    }

    public function prependMiddleware(Middleware $middleware) : void
    {
        $this->middlewares[] = $middleware;
    }

    public function dispatch(Message $message) : void
    {
        call_user_func($this->execute(), $message);
    }

    protected function execute() : Callable
    {
        $next = function () {};

        reset($this->middlewares);

        while ($middleware = current($this->middlewares)) {

            $next = function ($message) use ($middleware, $next) {
                $middleware->execute($message, $next);
            };

            next($this->middlewares);
        }

        return $next;
    }
}
