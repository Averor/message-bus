<?php declare(strict_types=1);

namespace Averor\MessageBus\Resolver;

use Averor\MessageBus\Contract\Message;
use Averor\MessageBus\Contract\NamedMessage;
use Averor\MessageBus\Contract\OneToOneResolver;
use Averor\MessageBus\Exception\MessageHandlerIsNotCallableException;
use Averor\MessageBus\Exception\MessageHandlerNotFoundException;

/**
 * Class CallableMapResolver
 *
 * @package Averor\MessageBus\Resolver
 * @author Averor <averor.dev@gmail.com>
 */
class CallableMapResolver implements OneToOneResolver
{
    /** @var array */
    protected $map = [];

    public function __construct(array $map)
    {
        $this->map = $map;
    }

    public function resolve(Message $message) : Callable
    {
        $name = $message instanceof NamedMessage
            ? $message->_name()
            : get_class($message);

        if (!array_key_exists($name, $this->map)) {
            throw new MessageHandlerNotFoundException(sprintf(
                "No handler found for message (%s)",
                $name
            ));
        }

        if (!is_callable($handler = $this->map[$name])) {
            throw new MessageHandlerIsNotCallableException(sprintf(
                "Given message (%s) handler is not callable",
                $name
            ));
        }

        return $handler;
    }
}
