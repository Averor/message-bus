<?php declare(strict_types=1);

namespace Averor\MessageBus\Resolver;

use Averor\MessageBus\Contract\Message;
use Averor\MessageBus\Contract\NamedMessage;
use Averor\MessageBus\Contract\OneToManyResolver;

/**
 * Class CallableCollectionResolver
 *
 * @package Averor\MessageBus\Resolver
 * @author Averor <averor.dev@gmail.com>
 */
class CallableCollectionResolver implements OneToManyResolver
{
    /** @var array */
    protected $map;

    public function __construct(array $map)
    {
        $this->map = $map;
    }

    public function resolve(Message $message) : array
    {
        $name = $message instanceof NamedMessage
            ? $message->_name()
            : get_class($message);

        if (!array_key_exists($name, $this->map)) {
            return [];
        }

        $handlers = $this->map[$name];

        return array_filter(
            $handlers,
            'is_callable'
        );
    }
}
