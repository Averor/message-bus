<?php declare(strict_types=1);

namespace Averor\MessageBus\Middleware;

use Averor\MessageBus\Contract\Message;
use Averor\MessageBus\Contract\Middleware;
use Averor\MessageBus\Contract\Publisher;

/**
 * Class PublishingMiddleware
 *
 * @package Averor\MessageBus\Middleware
 * @author Averor <averor.dev@gmail.com>
 */
class PublishingMiddleware implements Middleware
{
    /** @var Publisher */
    protected $publisher;

    public function __construct(Publisher $publisher)
    {
        $this->publisher = $publisher;
    }

    public function execute(Message $message, callable $next) : void
    {
        $this->publisher->publish($message);

        $next($message);
    }
}
