<?php declare(strict_types=1);

namespace Averor\MessageBus\Middleware;

use Averor\MessageBus\Contract\Message;
use Averor\MessageBus\Contract\Middleware;
use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;

/**
 * Class LoggingMiddleware
 *
 * @package Averor\MessageBus\Middleware
 * @author Averor <averor.dev@gmail.com>
 */
class LoggingMiddleware implements Middleware
{
    /** @var LoggerInterface */
    protected $logger;

    /** @var string */
    protected $level;

    public function __construct(LoggerInterface $logger, ?string $level = LogLevel::INFO)
    {
        $this->logger = $logger;
        $this->level = $level;
    }

    public function execute(Message $message, callable $next) : void
    {
        $this->logger->log(
            $this->level,
            sprintf(
                "Dispatching message %s",
                get_class($message)
            )
        );

        $next($message);

        $this->logger->log(
            $this->level,
            sprintf(
                "Message %s dispatched",
                get_class($message)
            )
        );
    }
}
