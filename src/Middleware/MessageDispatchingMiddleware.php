<?php declare(strict_types=1);

namespace Averor\MessageBus\Middleware;

use Averor\MessageBus\Contract\Dispatcher;
use Averor\MessageBus\Contract\Message;
use Averor\MessageBus\Contract\OneToManyResolver;
use Averor\MessageBus\Contract\OneToOneResolver;
use Averor\MessageBus\Contract\Resolver;

/**
 * Class MessageDispatchingMiddleware
 *
 * @package Averor\MessageBus\Middleware
 * @author Averor <averor.dev@gmail.com>
 */
class MessageDispatchingMiddleware implements Dispatcher
{
    /** @var Resolver */
    protected $resolver;

    public function __construct(Resolver $resolver)
    {
        $this->resolver = $resolver;
    }

    public function execute(Message $message, callable $next) : void
    {
        /** @var Callable|Callable[] $handler */
        $handler = $this->resolver->resolve($message);

        if ($this->resolver instanceof OneToOneResolver && is_callable($handler)) {
            $handler($message);
        } else if($this->resolver instanceof OneToManyResolver) {
            if (is_array($handler) && count($handler) > 0) {
                /** @var Callable $singleHandler */
                foreach ($handler as $singleHandler) {
                    $singleHandler($message);
                }
            }
        }

        $next($message);
    }
}
