<?php declare(strict_types=1);

namespace Averor\MessageBus\Middleware;

use Averor\MessageBus\Contract\Message;
use Averor\MessageBus\Contract\Middleware;
use Averor\MessageBus\Exception\MessageHandlingException;

/**
 * Class ExceptionUnifyingMiddleware
 *
 * @package Averor\MessageBus\Middleware
 * @author Averor <averor.dev@gmail.com>
 */
class ExceptionUnifyingMiddleware implements Middleware
{
    public function execute(Message $message, callable $next) : void
    {
        try {
            $next($message);
        } catch (\Throwable $e) {
            throw new MessageHandlingException(
                $e->getMessage(),
                $e->getCode(),
                $e
            );
        }
    }
}
