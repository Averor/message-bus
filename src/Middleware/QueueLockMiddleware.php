<?php declare(strict_types=1);

namespace Averor\MessageBus\Middleware;

use Averor\MessageBus\Contract\Message;
use Averor\MessageBus\Contract\Middleware;

/**
 * Class QueueLockMiddleware
 *
 * @package Averor\MessageBus\Middleware
 * @author Averor <averor.dev@gmail.com>
 */
class QueueLockMiddleware implements Middleware
{
    /** @var array */
    protected $queue = [];

    /** @var bool */
    protected $running = false;

    /**
     * @param Message $message
     * @param callable $next
     * @return void
     * @throws \Throwable
     */
    public function execute(Message $message, callable $next) : void
    {
        $this->queue[] = $message;

        if (!$this->running) {

            $this->running = true;

            while ($message = array_shift($this->queue)) {
                try {
                    $next($message);
                } catch (\Throwable $exception) {
                    $this->running = false;

                    throw $exception;
                }
            }

            $this->running = false;
        }
    }
}
