<?php declare(strict_types=1);

namespace Averor\MessageBus\Contract;

/**
 * Interface MessageWithDate
 *
 * @package Averor\MessageBus\Contract
 * @author Averor <averor.dev@gmail.com>
 */
interface MessageWithDate
{
    public function _date() : \DateTimeInterface;
}
