<?php declare(strict_types=1);

namespace Averor\MessageBus\Contract;

/**
 * Interface Message
 *
 * @package Averor\MessageBus\Contract
 * @author Averor <averor.dev@gmail.com>
 */
interface Message
{
}
