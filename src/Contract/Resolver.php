<?php declare(strict_types=1);

namespace Averor\MessageBus\Contract;

/**
 * Interface Resolver
 *
 * @package Averor\MessageBus\Contract
 * @author Averor <averor.dev@gmail.com>
 */
interface Resolver
{
    public function resolve(Message $message);
}
