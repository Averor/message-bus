<?php declare(strict_types=1);

namespace Averor\MessageBus\Contract;

/**
 * Interface MessageWithMetadata
 *
 * @package Averor\MessageBus\Contract
 * @author Averor <averor.dev@gmail.com>
 */
interface MessageWithMetadata
{
    public function _metadata() : MessageMetadata;
}
