<?php declare(strict_types=1);

namespace Averor\MessageBus\Contract;

/**
 * Interface Identifier
 *
 * @package Averor\MessageBus\Contract
 * @author Averor <averor.dev@gmail.com>
 */
interface Identifier
{
    public static function create() : Identifier;

    public static function fromString(string $identifier) : Identifier;

    public function __toString() : string;

    public function toString() : string;
}
