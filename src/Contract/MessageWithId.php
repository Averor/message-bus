<?php declare(strict_types=1);

namespace Averor\MessageBus\Contract;

/**
 * Interface MessageWithId
 *
 * @package Averor\MessageBus\Contract
 * @author Averor <averor.dev@gmail.com>
 */
interface MessageWithId
{
    public function _id() : string;
}
