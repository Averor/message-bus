<?php declare(strict_types=1);

namespace Averor\MessageBus\Contract;

/**
 * Interface Middleware
 *
 * @package Averor\MessageBus\Contract
 * @author Averor <averor.dev@gmail.com>
 */
interface Middleware
{
    public function execute(Message $message, callable $next) : void;
}
