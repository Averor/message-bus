<?php declare(strict_types=1);

namespace Averor\MessageBus\Contract;

/**
 * Interface OneToOneResolver
 *
 * @package Averor\MessageBus\Contract
 * @author Averor <averor.dev@gmail.com>
 */
interface OneToOneResolver extends Resolver
{
    public function resolve(Message $message) : Callable;
}
