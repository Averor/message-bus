<?php declare(strict_types=1);

namespace Averor\MessageBus\Contract;

/**
 * Interface Publisher
 *
 * @package Averor\MessageBus\Contract
 * @author Averor <averor.dev@gmail.com>
 */
interface Publisher
{
    public function publish(Message $message) : void;
}
