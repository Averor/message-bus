<?php declare(strict_types=1);

namespace Averor\MessageBus\Contract;

/**
 * Interface NamedMessage
 *
 * @package Averor\MessageBus\Contract
 * @author Averor <averor.dev@gmail.com>
 */
interface NamedMessage
{
    public function _name() : string;
}
