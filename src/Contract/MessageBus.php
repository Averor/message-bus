<?php declare(strict_types=1);

namespace Averor\MessageBus\Contract;

/**
 * Interface MessageBus
 *
 * @package Averor\MessageBus\Contract
 * @author Averor <averor.dev@gmail.com>
 */
interface MessageBus
{
    public function dispatch(Message $message) : void;
}
