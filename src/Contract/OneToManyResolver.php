<?php declare(strict_types=1);

namespace Averor\MessageBus\Contract;

/**
 * Interface OneToManyResolver
 *
 * @package Averor\MessageBus\Contract
 * @author Averor <averor.dev@gmail.com>
 */
interface OneToManyResolver extends Resolver
{
    /**
     * @param Message $message
     * @return Callable[]
     */
    public function resolve(Message $message) : array;
}
