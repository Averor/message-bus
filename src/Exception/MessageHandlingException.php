<?php declare(strict_types=1);

namespace Averor\MessageBus\Exception;

/**
 * Class MessageHandlingException
 *
 * @package Averor\MessageBus\Exception
 * @author Averor <averor.dev@gmail.com>
 */
class MessageHandlingException extends \RuntimeException
{
}
