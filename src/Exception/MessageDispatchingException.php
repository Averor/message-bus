<?php declare(strict_types=1);

namespace Averor\MessageBus\Exception;

/**
 * Class MessageDispatchingException
 *
 * @package Averor\MessageBus\Exception
 * @author Averor <averor.dev@gmail.com>
 */
class MessageDispatchingException extends \LogicException
{
}
