<?php declare(strict_types=1);

namespace Averor\MessageBus\Exception;

/**
 * Class MessageHandlerNotFoundException
 *
 * @package Averor\MessageBus\Exception
 * @author Averor <averor.dev@gmail.com>
 */
class MessageHandlerNotFoundException extends MessageDispatchingException
{
}
