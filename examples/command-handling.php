<?php declare(strict_types=1);

use Averor\MessageBus\Contract\Command;
use Averor\MessageBus\Contract\CommandHandler;
use Averor\MessageBus\MessageBus;
use Averor\MessageBus\Middleware\ExceptionUnifyingMiddleware;
use Averor\MessageBus\Middleware\LoggingMiddleware;
use Averor\MessageBus\Middleware\MessageDispatchingMiddleware;
use Averor\MessageBus\Middleware\QueueLockMiddleware;
use Averor\MessageBus\Resolver\CallableMapResolver;
use Averor\SimpleLogger\Logger;
use Psr\Log\LogLevel;

require __DIR__ . '/../vendor/autoload.php';

class SomeCommand implements Command
{
    public $id, $name;
    public function __construct(string $id, string $name)
    {
        $this->id = $id;
        $this->name = $name;
    }
}

class SomeCommandHandler implements CommandHandler
{
    public function __invoke(SomeCommand $command) : void
    {
        printf(
            "Invoked %s with params: id = %s and name = %s\n",
            __METHOD__,
            $command->id,
            $command->name
        );
    }
}

$commandsMap = [
    SomeCommand::class => new SomeCommandHandler()
];

$commandBus = new MessageBus([
    new LoggingMiddleware(
        new Logger('php://output'),
        LogLevel::INFO
    ),
    new ExceptionUnifyingMiddleware()
]);

$commandBus->prependMiddleware(
    new QueueLockMiddleware()
);

// Pay attention to properly positioning of Message Dispatcher in middleware stack, it always depends on use-case.
// In this sample case it is important to place it as the last executed middleware.
// If e.g. LoggingMiddleware was executed later, it would not be possible to log entry before message is being handled
// Same goes with ExceptionUnifyingMiddleware, it must be registered earlier than Dispatcher
$commandBus->appendMiddleware(
    new MessageDispatchingMiddleware(
        new CallableMapResolver($commandsMap)
    )
);

$commandBus->dispatch(
    new SomeCommand('foo', 'bar')
);
